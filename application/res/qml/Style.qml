pragma Singleton
import QtQuick 2.0
QtObject {
    readonly property color darkDarkGray: "#232323"
    readonly property color darkGray: "#474747"
    readonly property color gray: "#a7a7a7"
    readonly property color lightGray: "#c9c9c9"
    readonly property color lightGreen: "#97FFAF"

    readonly property double lightBulbWidthScaler: 1.4
    readonly property double lightBulbHeightScaler: 1.2

    readonly property color primary: "#000000"
    readonly property color accent: "DarkOrange"
}
