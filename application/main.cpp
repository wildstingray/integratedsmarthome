#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QVariantList>
#include "devicetype.h"
#include "smartdevicesmodel.h"
#include "globalproperties.h"
#include "qmlmqttclient.h"
#include "jsonfilehandler.h"

int main(int argc, char *argv[])
{
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));
//    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    //C++
    JsonFileHandler fileHandler(&app);
    SmartDevicesModel smartDevicesModel(&app);
    smartDevicesModel.setJsonHandlerPtr(&fileHandler);
    GlobalProperties globalProperties(&app); //TODO make this useful

    //QML
    QQmlApplicationEngine engine;

    qmlRegisterType<SmartDevicesModel>("com.integratedSmartHome", 1, 0, "SmartDevicesModel");
    qmlRegisterType<QmlMqttClient>("com.integratedSmartHome", 1, 0, "MqttClient");
    qmlRegisterUncreatableType<QmlMqttSubscription>("com.integratedSmartHome", 1, 0, "MqttSubscription", QLatin1String("Read only, no copy copy"));

    //Exposing the "DeviceType" class to qml might be helpful but it is fine for now

    QQmlContext *context = engine.rootContext();
        context->setContextProperty("smartDevicesModel", &smartDevicesModel);
        context->setContextProperty("globalProperties", &globalProperties);


    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;


    return app.exec();

}
